# fdrubigny

Clef publique : 32jZNQLKYfW9KtCHiaSewR27ZRb6zoncC6JvBVCBW4k1

## Mes contributions au sein de la Monnaie Libre
- Réalisation et maintenance du site Airbnjune.org https://airbnjune.org
- Réalisation et maintenance du site Juneted https://juneted.g1.lu 
- Réalisation du site g1.lu et fourniture de sous-domaines en g1.lu https://g1.lu
- Hébergeur d'un noeud Duniter V2S Mirror duniter-v2s-adn-life_mirror
- Participation à la rédaction du quid de la demande de certification https://git.duniter.org/fdrubigny/g1-quid-de-la-demande-de-certification./-/blob/main/Quid_de_la_demande_de_certification.md?ref_type=heads
